variable "env" {
  default = "dev"
}

# VPC ID
variable "main_vpc" {
  default = ""
}

variable "cidr_vpc" {
  default = ""
}

variable "es_instance_type" {
  default = "t3.medium.elasticsearch"
}
variable "es_instance_count" {
  default = "2"
}
variable "es_master_enable" {
  default = "true"
} 
variable "es_master_instance_type" {
  default = ""
}
variable "es_master_instance_count" {
  default = "1"
}
variable "es_zone_awareness" {
  default = "false"
}
variable "es_volume_size" {
  default = "20"
}
variable "es_subnet1" {
  default = ""
}
variable "aws_account_id" {
  default = ""
}
variable "es_domain" {
  default = "mydomain"
}