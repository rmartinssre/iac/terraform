# Terraform AWS infrastructure deploy

##### Requirements

- terraform 12x (https://releases.hashicorp.com/terraform/)
- AWS Credentials configuration (https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)
- A basic VPC in AWS, with one at least two public or private subnets

### Terraform Configuration


```bash
terraform init
terraform plan
```

### Remote state

In order to configure your environment for all team, you should not write your state locally. For that, is necessary to configure your remote state. Example: S3, AzureBlob, Consul.

Pay attention in lock for this state, more information: https://www.terraform.io/docs/backends/types/s3.html

### This project will be create

- An elasticsearch domain on aws, with one master node and two datanodes
- Terraform remote state, wich will create a tsfate file in a Bucket S3
- Default iam role to be used for yourcluster


