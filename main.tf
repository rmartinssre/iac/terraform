## AWS Fundacao
## Ok

provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}


# Create a s3 bucket in order to write a remote tfstate
terraform {
  backend "s3" {
    bucket = "yours3bucket"
    key    = "terraform/"
    region = "us-east-1"
  }
}

resource "aws_security_group" "elasticsearch-sg" { 
  name        = "${var.env}-es-scg" 
  description = "Elasticsearch Service SG" 
  vpc_id      = "${var.main_vpc}" 
 
  ingress { 
    from_port = 22
    to_port   = 22 
    protocol  = "tcp" 
 
    cidr_blocks = [ 
      "0.0.0.0/0", 
    ] 
  } 

  ingress { 
    from_port = 443 
    to_port   = 443 
    protocol  = "tcp" 
 
    cidr_blocks = [ 
      "0.0.0.0/0", 
    ] 
  } 
    ingress { 
    from_port = 0 
    to_port   = 0 
    protocol  = "-1" 
 
        cidr_blocks = [ 
      "${var.cidr_vpc}", 
    ] 
  } 
  egress { 
    protocol    = "-1" 
    from_port   = 0 
    to_port     = 0 
    cidr_blocks = ["0.0.0.0/0"] 
  } 
   
} 

# LogGroup do ElasticSearch 
resource "aws_cloudwatch_log_group" "elasticsearch-logs" { 
  name = "${var.env}-elasticsearch-logs" 
} 
 
resource "aws_cloudwatch_log_resource_policy" "elasticsearch-log-policy" { 
  policy_name = "${var.env}-elasticsearch-log-policy"  
  policy_document = <<EOF
{ 
  "Version": "2012-10-17", 
  "Statement": [ 
    { 
      "Effect": "Allow", 
      "Principal": { 
        "Service": "es.amazonaws.com" 
      }, 
      "Action": [ 
        "logs:PutLogEvents", 
        "logs:PutLogEventsBatch", 
        "logs:CreateLogStream" 
      ], 
      "Resource": "arn:aws:logs:*" 
    } 
  ] 
} 
EOF
} 
 
resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}


resource "aws_elasticsearch_domain" "pocsre" { 
  domain_name           = "${var.es_domain}" 
  elasticsearch_version = "7.1" 
 
  cluster_config { 
    instance_type = "${var.es_instance_type}" 
    instance_count = "${var.es_instance_count}"  
#    dedicated_master_enabled = "${var.es_master_enable}" 
#    dedicated_master_type = "${var.es_master_instance_type}" 
#    dedicated_master_count = "${var.es_master_instance_count}" 
    zone_awareness_enabled = "${var.es_zone_awareness}" 
  } 
 
   ebs_options { 
     ebs_enabled = true 
     volume_size = "${var.es_volume_size}" 
   } 
 
  vpc_options { 
    subnet_ids = [ 
       "${var.es_subnet1}"
    ] 
 
    security_group_ids = ["${aws_security_group.elasticsearch-sg.id}"]

  } 
 
  advanced_options = { 
    "rest.action.multi.allow_explicit_index" = "true" 
  } 
 
  access_policies = <<EOF
{ 
  "Version": "2012-10-17", 
  "Statement": [ 
    { 
      "Effect": "Allow", 
      "Principal": { 
        "AWS": "*" 
      }, 
      "Action": "es:*", 
      "Resource": "arn:aws:es:us-east-1:${var.aws_account_id}:domain/${var.es_domain}/*" 
    } 
  ] 
} 
EOF
 
  log_publishing_options { 
    cloudwatch_log_group_arn = "${aws_cloudwatch_log_group.elasticsearch-logs.arn}" 
    log_type                 = "ES_APPLICATION_LOGS" 
  }

   depends_on = [
    "aws_iam_service_linked_role.es",
  ] 
} 
